﻿using OItemFilterModule.Models;
using OItemFilterModule.Services;
using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemFilterModule.Factories
{
    public class OItemFilterFactory
    {
        private Globals globals;

        public async Task<ResultItems> CreateResultSearchItems(ResultSearchItem resultSearchItem)
        {
            var taskResult = await Task.Run<ResultItems>(() =>
            {
                var result = new ResultItems
                {
                    Result = globals.LState_Success.Clone()
                };

                if (resultSearchItem.ItemType == globals.Type_AttributeType)
                {
                    result.Items = resultSearchItem.SimpleItems.Select(searchItm => new ResultItem
                    {
                        IdItem = searchItm.GUID,
                        NameItem = searchItm.Name,
                        IdParent = searchItm.GUID_Parent,
                        NameParent = searchItm.Name_Parent,
                        Type = globals.Type_AttributeType
                    }).ToList();
                }
                else if (resultSearchItem.ItemType == globals.Type_RelationType)
                {
                    result.Items = resultSearchItem.SimpleItems.Select(searchItm => new ResultItem
                    {
                        IdItem = searchItm.GUID,
                        NameItem = searchItm.Name,
                        Type = globals.Type_RelationType
                    }).ToList();
                }
                else if (resultSearchItem.ItemType == globals.Type_Class)
                {
                    result.Items = resultSearchItem.SimpleItems.Select(searchItm => new ResultItem
                    {
                        IdItem = searchItm.GUID,
                        NameItem = searchItm.Name,
                        IdParent = searchItm.GUID_Parent,
                        NameParent = searchItm.Name_Parent,
                        Type = globals.Type_Class
                    }).ToList();
                }
                else if (resultSearchItem.ItemType == globals.Type_Object)
                {
                    result.Items = (from simpleItem in resultSearchItem.SimpleItems
                                    join classItem in resultSearchItem.Classes on simpleItem.GUID_Parent equals classItem.GUID
                                    select new ResultItem
                                    {
                                        IdItem = simpleItem.GUID,
                                        NameItem = simpleItem.Name,
                                        IdParent = simpleItem.GUID_Parent,
                                        NameParent = classItem.Name,
                                        Type = globals.Type_Object
                                    }).ToList();

                }
                else if (resultSearchItem.ItemType == globals.Type_ObjectAtt)
                {
                    result.Items = resultSearchItem.AttItems.Select(searchItm => new ResultItem
                    {
                        IdItem = searchItm.ID_Object,
                        NameItem = searchItm.Name_Object,
                        IdParent = searchItm.ID_Class,
                        NameParent = searchItm.Name_Class,
                        IdAttribute = searchItm.ID_Attribute,
                        IdAttributeType = searchItm.ID_AttributeType,
                        NameAttributeType = searchItm.Name_AttributeType,
                        ValueNamed = searchItm.Val_Named,
                        Type = globals.Type_ObjectAtt
                    }).ToList();
                }
                else if (resultSearchItem.ItemType == globals.Type_ObjectRel)
                {
                    if (resultSearchItem?.Direction.GUID == globals.Directions.Direction_RightLeft.GUID)
                    {
                        result.Items = resultSearchItem.RelItems.Select(searchItm => new ResultItem
                        {
                            IdItem = searchItm.ID_Other,
                            NameItem = searchItm.Name_Other,
                            IdParent = searchItm.ID_Parent_Other,
                            NameParent = searchItm.Name_Parent_Other,
                            Type = globals.Type_ObjectRel
                        }).ToList();
                    }
                    else
                    {
                        result.Items = resultSearchItem.RelItems.Select(searchItm => new ResultItem
                        {
                            IdItem = searchItm.ID_Object,
                            NameItem = searchItm.Name_Object,
                            IdParent = searchItm.ID_Parent_Object,
                            NameParent = searchItm.Name_Parent_Object,
                            Type = globals.Type_ObjectRel
                        }).ToList();
                    }

                }
                else
                {
                    result.Result = globals.LState_Error.Clone();
                }


                return result;
            });

            return taskResult;
        }

        public OItemFilterFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}

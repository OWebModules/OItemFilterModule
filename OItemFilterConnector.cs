﻿using OItemFilterModule.Factories;
using OItemFilterModule.Models;
using OItemFilterModule.Services;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemFilterModule
{
    public class OItemFilterConnector : AppController
    {
        public Globals Globals { get; private set; }

        private ServiceAgentElastic serviceAgentElastic;
        private OItemFilterFactory factory;
        private object agentLocker = new object();

        public ResultSimpleItem resultGetSearchItem;
        public ResultSimpleItem ResultGetSearchItem
        {
            get
            {
                lock(agentLocker)
                {
                    return resultGetSearchItem;
                }
            }
            set
            {
                lock(agentLocker)
                {
                    resultGetSearchItem = value;
                }
            }
        }

        public ResultItems resultSearchItem;
        public ResultItems ResultSearchItem
        {
            get
            {
                lock (agentLocker)
                {
                    return resultSearchItem;
                }
            }
            set
            {
                lock (agentLocker)
                {
                    resultSearchItem = value;
                }
            }
        }

        public async Task<ResultSimpleItem> GetSearchItem(SearchItem sourceItem)
        {
            var result = new ResultSimpleItem
            {
                Result = Globals.LState_Success.Clone()
            };
            if (sourceItem.type == "attributeTypeButton")
            {
                var resultTask = await serviceAgentElastic.GetAttributeTypes(sourceItem);

                result.AttributeTypes = resultTask.SimpleItem.LookupItems;
                sourceItem.dropDownConfig = new KendoDropDownConfig
                {
                    dataSource = resultTask.SimpleItem.LookupItems.OrderBy(lookItm => lookItm.Name).Select(attType => new KendoDropDownItem
                    {
                        Value = attType.GUID,
                        Text = attType.Name
                    }).ToList(),
                    optionLabel = "Attribute-Type"

                };
            }
            else if (sourceItem.type == "relationTypeButton")
            {
                var resultTask = await serviceAgentElastic.GetRelationTypes(sourceItem);

                result.RelationTypes = resultTask.SimpleItem.LookupItems;
                sourceItem.dropDownConfig = new KendoDropDownConfig
                {
                    dataSource = resultTask.SimpleItem.LookupItems.OrderBy(lookItm => lookItm.Name).Select(attType => new KendoDropDownItem
                    {
                        Value = attType.GUID,
                        Text = attType.Name
                    }).ToList(),
                    optionLabel = "Relation-Type"

                };
            }
            else if (sourceItem.type == "classButton")
            {
                var resultTask = await serviceAgentElastic.GetClasses(sourceItem);

                result.Classes = resultTask.SimpleItem.LookupItems;
                sourceItem.dropDownConfig = new KendoDropDownConfig
                {
                    dataSource = resultTask.SimpleItem.LookupItems.OrderBy(lookItm => lookItm.Name).Select(attType => new KendoDropDownItem
                    {
                        Value = attType.GUID,
                        Text = attType.Name
                    }).ToList(),
                    optionLabel = "Class"

                };
            }

            result.SearchItem = sourceItem;
            ResultGetSearchItem = result;
            return result;
        }

        public async Task<ResultItems> SearchItems(List<SearchItem> searchItems, List<clsOntologyItem> classes)
        {
            
            var resultTask = await serviceAgentElastic.SearchItems(searchItems, classes);

            var resultFactoryTask = await factory.CreateResultSearchItems(resultTask);

            ResultSearchItem = resultFactoryTask;
            return resultFactoryTask;
        }

       
        public OItemFilterConnector(Globals globals) : base(globals)
        {
            Globals = globals;
            Initialize();
        }

        private void Initialize()
        {
            serviceAgentElastic = new ServiceAgentElastic(Globals);
            factory = new OItemFilterFactory(Globals);
        }
    }

    public class ResultSimpleItem
    {
        public clsOntologyItem Result { get; set; }
        public SearchItem SearchItem { get; set; }
        public List<clsOntologyItem> RelationTypes { get; set; }
        public List<clsOntologyItem> AttributeTypes { get; set; }
        public List<clsOntologyItem> Classes { get; set; }
    }

    public class ResultItems
    {
        public clsOntologyItem Result { get; set; }
        public List<ResultItem> Items { get; set; }
    }
}

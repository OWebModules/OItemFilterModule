﻿using OItemFilterModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemFilterModule.Services
{
    public class ServiceAgentElastic : NotifyPropertyChange
    {
        private Globals globals;

        private object agentLocker = new object();

        private ResultAttributeList resultAttributeList;

        public ResultAttributeList ResultAttributeList
        {
            get
            {
                lock(agentLocker)
                {
                    return resultAttributeList;
                }
            }
            private set
            {
                lock(agentLocker)
                {
                    resultAttributeList = value;
                }
                RaisePropertyChanged(nameof(ResultAttributeList));
            }
        }
        List<string> namesList = new List<string>();

        private ResultSimpleItem resultAttributeTypeList;
        public ResultSimpleItem ResultAttributeTypeList
        {
            get
            {
                lock (agentLocker)
                {
                    return resultAttributeTypeList;
                }
                    
            }
            set
            {
                lock (agentLocker)
                {
                    resultAttributeTypeList = value;
                }
                    
                RaisePropertyChanged(nameof(ResultAttributeTypeList));
            }
        }

        private ResultSimpleItem resultClassList;
        public ResultSimpleItem ResultClassList
        {
            get
            {
                lock (agentLocker)
                {
                    return resultClassList;
                }
                    
            }
            set
            {
                lock (agentLocker)
                {
                    resultClassList = value;
                }
                    
                RaisePropertyChanged(nameof(ResultClassList));
            }
        }

        private ResultClassList resultFilteredClassList;
        public ResultClassList ResultFilteredClassList
        {
            get
            {
                lock (agentLocker)
                {
                    return resultFilteredClassList;
                }

            }
            set
            {
                lock (agentLocker)
                {
                    resultFilteredClassList = value;
                }

                RaisePropertyChanged(nameof(ResultFilteredClassList));
            }
        }

        private ResultSimpleItem resultRelationTypeList;
        public ResultSimpleItem ResultRelationTypeList
        {
            get
            {
                lock (agentLocker)
                {
                    return resultRelationTypeList;
                }
                    
            }
            set
            {
                lock (agentLocker)
                {
                    resultRelationTypeList = value;
                }
                    
                RaisePropertyChanged(nameof(ResultRelationTypeList));
            }
        }

        private ResultSimpleItem resultObjectList;
        public ResultSimpleItem ResultObjectList
        {
            get
            {
                lock (agentLocker)
                {
                    return resultObjectList;
                }
                    
            }
            set
            {
                lock (agentLocker)
                {
                    resultObjectList = value;
                }
                    
                RaisePropertyChanged(nameof(ResultObjectList));
            }
        }

        private ResultObjectList resultFilteredObjectList;
        public ResultObjectList ResultFilteredObjectList
        {
            get
            {
                lock(agentLocker)
                {
                    return resultFilteredObjectList;
                }
            }
            set
            {
                lock(agentLocker)
                {
                    resultFilteredObjectList = value;
                }
                RaisePropertyChanged(nameof(ResultFilteredObjectList));
            }
        }

        private LookupRelItem resultObjectRelList;
        public LookupRelItem ResultObjectRelList
        {
            get
            {
                lock (agentLocker)
                {
                    return resultObjectRelList;
                }
            }
            set
            {
                lock (agentLocker)
                {
                    resultObjectRelList = value;
                }
                RaisePropertyChanged(nameof(ResultObjectRelList));
            }
        }

        private clsOntologyItem oItem;
        public clsOntologyItem OItem
        {
            get
            {
                lock (agentLocker)
                {
                    return oItem;
                }
            }
            private set
            {
                lock (agentLocker)
                {
                    oItem = value;
                }
                RaisePropertyChanged(nameof(OItem));
            }
        }


        public async Task<ResultSimpleItem> GetClasses(SearchItem searchItem)
        {
            var taskResult = await Task.Run<ResultSimpleItem>(() =>
            {
                var dbReaderObjects = new OntologyModDBConnector(globals);
                var result = dbReaderObjects.GetDataClasses();

                LookupSimpleItem lookupItem = null;
                if (result.GUID == globals.LState_Success.GUID)
                {
                    lookupItem = new LookupSimpleItem
                    {
                        LookupItems = dbReaderObjects.Classes1,
                        SearchItem = searchItem
                    };


                }
                var resultSimple = new ResultSimpleItem
                {
                    Result = globals.LState_Success.Clone(),
                    SimpleItem = lookupItem
                };

                ResultClassList = resultSimple;
                return resultSimple;
            });

            return taskResult;
        }

        public async Task<ResultClassList> GetClassList(List<clsOntologyItem> classSearch, bool triggerResult = true)
        {
            var taskResult = await Task.Run<ResultClassList>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);


                var result = new ResultClassList
                {
                    Result = dbReader.GetDataClasses(classSearch)
                };
                result.Classes = dbReader.Classes1;

                if (triggerResult)
                {
                    ResultFilteredClassList = result;
                }

                return result;
            });
            return taskResult;
        }

        public async Task<ResultSimpleItem> GetAttributeTypes(SearchItem searchItem)
        {
            var taskResult = await Task.Run<ResultSimpleItem>(() =>
            {
                var dbReaderObjects = new OntologyModDBConnector(globals);
                var result = dbReaderObjects.GetDataAttributeType();

                LookupSimpleItem lookupItem = null;
                if (result.GUID == globals.LState_Success.GUID)
                {
                    lookupItem = new LookupSimpleItem
                    {
                        LookupItems = dbReaderObjects.AttributeTypes,
                        SearchItem = searchItem
                    };


                }

                var resultSimple = new ResultSimpleItem
                {
                    Result = globals.LState_Success.Clone(),
                    SimpleItem = lookupItem
                };
                ResultAttributeTypeList = resultSimple;
                return resultSimple;
            });

            return taskResult;
        }

        public async Task<ResultAttributeList> GetAttributeList(List<clsObjectAtt> attSearch)
        {
            var taskResult = await Task.Run<ResultAttributeList>(() =>
            {
                var dbReaderAttributes = new OntologyModDBConnector(globals);

                var result = new ResultAttributeList
                {
                    Result = dbReaderAttributes.GetDataObjectAtt(attSearch)
                };

                result.ObjectAttributes = dbReaderAttributes.ObjAtts;
                ResultAttributeList = result;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultSimpleItem> GetRelationTypes(SearchItem searchItem)
        {
            var taskResult = await Task.Run<ResultSimpleItem>(() =>
            {
                var dbReaderObjects = new OntologyModDBConnector(globals);
                var result = dbReaderObjects.GetDataRelationTypes(null);

                LookupSimpleItem lookupItem = null;
                if (result.GUID == globals.LState_Success.GUID)
                {
                    lookupItem = new LookupSimpleItem
                    {
                        LookupItems = dbReaderObjects.RelationTypes,
                        SearchItem = searchItem
                    };


                }

                var resultSimple = new ResultSimpleItem
                {
                    Result = globals.LState_Success.Clone(),
                    SimpleItem = lookupItem
                };
                ResultRelationTypeList = resultSimple;
                return resultSimple;
            });

            return taskResult;
        }

        public async Task<ResultObjectList> GetObjectList(List<clsOntologyItem> objectSearch)
        {
            var taskResult = await Task.Run<ResultObjectList>(() =>
            {
                var dbReaderObjects = new OntologyModDBConnector(globals);
                var result = new ResultObjectList
                {
                    Result = dbReaderObjects.GetDataObjects(objectSearch)
                };

                result.Objects = dbReaderObjects.Objects1;

                ResultFilteredObjectList = result;
                return result;
            });
            return taskResult;   
        }

        public async Task<LookupRelItem> GetObjectRelations(List<clsObjectRel> search, clsOntologyItem direction)
        {
            var taskResult = await Task.Run<LookupRelItem>(() =>
            {
                var dbReaderObjectRels = new OntologyModDBConnector(globals);
                var result = new LookupRelItem
                {
                    Result = dbReaderObjectRels.GetDataObjectRel(search),
                    Direction = direction
                };

                result.LookupItems = dbReaderObjectRels.ObjectRels;

                ResultObjectRelList = result;
                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> GetOItem(string id, string type)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);
                OItem = dbReader.GetOItem(id, type);
                return oItem;
            });

            return taskResult;
        }

        public async Task<ResultSearchItem> SearchItems(List<SearchItem> searchItems, List<clsOntologyItem> classes)
        {
            var searchItem = searchItems.FirstOrDefault();

            var resultSearch = new ResultSearchItem
            {
                Result = globals.LState_Success.Clone()
            };

            if (searchItem != null)
            {
                if (string.IsNullOrEmpty(searchItem.type))
                {
                    resultSearch.ItemType = globals.Type_Object;
                    resultSearch.SimpleItems = new List<clsOntologyItem>();
                    resultSearch.Classes = new List<clsOntologyItem>();
                    return resultSearch;
                }
            }
            var taskResult = await Task.Run<ResultSearchItem>(async () =>
            {
                

                var classSearch = new List<clsOntologyItem>();
                var objectSearch = new List<clsOntologyItem>();
                var relationTypeSearch = new List<clsOntologyItem>();
                var attributeTypeSearch = new List<clsOntologyItem>();
                var objectRelSearch = new List<clsObjectRel>();
                var objectAttSearch = new List<clsObjectAtt>(); ;

                var direction = globals.Direction_LeftRight;

                if (classes == null || !classes.Any())
                {
                    var getClassesTask = await GetClassList(classSearch: null);
                    classes = getClassesTask.Classes;
                }

                resultSearch.Classes = classes;

                if (searchItems.Any(searchItm => searchItm.type == "rightLeftButton"))
                {
                    direction = globals.Direction_RightLeft;
                }

                searchItems.ForEach(searchItm =>
                {
                    if (searchItm.type == "objectButton")
                    {
                        searchItm.OrderId = 10;
                    }
                    else if (searchItm.type == "leftRightButton")
                    {
                        searchItm.OrderId = 15;
                    }
                    else if (searchItm.type == "rightLeftButton")
                    {
                        searchItm.OrderId = 15;
                    }

                });

                searchItems.OrderBy(searchItm => searchItm.OrderId).ToList().ForEach(searchItm =>
                {
                    if (searchItm.type == "classButton")
                    {
                        if (!string.IsNullOrEmpty(searchItm.idClass))
                        {
                            classSearch.Add(new clsOntologyItem
                            {
                                GUID_Parent = searchItm.idClass
                            });
                        }
                        else if (!string.IsNullOrEmpty(searchItm.searchText) && globals.is_GUID(searchItm.searchText))
                        {
                            classSearch.Add(new clsOntologyItem
                            {
                                GUID_Parent = searchItm.searchText
                            });
                        }

                    }
                    else if (searchItm.type == "objectButton")
                    {
                        if (!string.IsNullOrEmpty(searchItm.searchText))
                        {
                            if (classSearch.Any())
                            {
                                objectSearch.AddRange(classSearch.Select(cls => new clsOntologyItem
                                {
                                    GUID = globals.is_GUID(searchItm.searchText) ? searchItm.searchText : null,
                                    Name = globals.is_GUID(searchItm.searchText) ? null : searchItm.searchText,
                                    GUID_Parent = cls.GUID_Parent
                                }));
                            }
                            else
                            {
                                objectSearch.Add(new clsOntologyItem
                                {
                                    GUID = globals.is_GUID(searchItm.searchText) ? searchItm.searchText : null,
                                    Name = globals.is_GUID(searchItm.searchText) ? null : searchItm.searchText,
                                });
                            }
                        }
                    }
                    else if (searchItm.type == "relationTypeButton")
                    {
                        relationTypeSearch.Add(new clsOntologyItem
                        {
                            GUID = searchItm.idRelationType
                        });
                    }
                    else if (searchItm.type == "attributeTypeButton")
                    {
                        var attributeTypeTask = GetOItem(searchItm.idAttributeType, globals.Type_AttributeType);
                        attributeTypeTask.Wait();
                        var attributeType = attributeTypeTask.Result;
                        if (attributeType.GUID_Parent == globals.DType_Bool.GUID)
                        {
                            attributeTypeSearch.Add(new clsOntologyItem
                            {
                                GUID = attributeType.GUID,
                                GUID_Parent = attributeType.GUID_Parent,
                                Val_Bool = searchItm.boolValue
                            });
                        }
                        else if (attributeType.GUID_Parent == globals.DType_DateTime.GUID)
                        {
                            attributeTypeSearch.Add(new clsOntologyItem
                            {
                                GUID = attributeType.GUID,
                                GUID_Parent = attributeType.GUID_Parent,
                                Val_Date = searchItm.dateTimeValue
                            });
                        }
                        else if (attributeType.GUID_Parent == globals.DType_Int.GUID)
                        {
                            attributeTypeSearch.Add(new clsOntologyItem
                            {
                                GUID = attributeType.GUID,
                                GUID_Parent = attributeType.GUID_Parent,
                                Val_Long = searchItm.intValue
                            });
                        }
                        else if (attributeType.GUID_Parent == globals.DType_Real.GUID)
                        {
                            attributeTypeSearch.Add(new clsOntologyItem
                            {
                                GUID = attributeType.GUID,
                                GUID_Parent = attributeType.GUID_Parent,
                                Val_Real = searchItm.dblValue
                            });
                        }
                        else if (attributeType.GUID_Parent == globals.DType_String.GUID)
                        {
                            attributeTypeSearch.Add(new clsOntologyItem
                            {
                                GUID = attributeType.GUID,
                                GUID_Parent = attributeType.GUID_Parent,
                                Val_String = searchItm.strValue
                            });
                        }
                    }
                    else if (searchItm.type == "leftRightButton")
                    {
                        if (!string.IsNullOrEmpty(searchItm.searchText))
                        {
                            if (!globals.is_GUID(searchItm.searchText))
                            {
                                namesList.Add(searchItm.searchText.ToLower());
                            }

                            if (classSearch.Any())
                            {
                                objectRelSearch.AddRange(classSearch.Select(cls => new clsObjectRel
                                {
                                    ID_Other = globals.is_GUID(searchItm.searchText) ? searchItm.searchText : null,
                                    Name_Other = globals.is_GUID(searchItm.searchText) ? null : searchItm.searchText,
                                    ID_Parent_Other = cls.GUID_Parent
                                }));
                            }
                            else
                            {
                                objectRelSearch.Add(new clsObjectRel
                                {
                                    ID_Other = globals.is_GUID(searchItm.searchText) ? searchItm.searchText : null,
                                    Name_Other = globals.is_GUID(searchItm.searchText) ? null : searchItm.searchText,
                                });
                            }
                        }

                    }
                    else if (searchItm.type == "rightLeftButton")
                    {
                        if (!string.IsNullOrEmpty(searchItm.searchText))
                        {
                            if (!globals.is_GUID(searchItm.searchText))
                            {
                                namesList.Add(searchItm.searchText.ToLower());
                            }

                            if (classSearch.Any())
                            {
                                objectRelSearch.AddRange(classSearch.Select(cls => new clsObjectRel
                                {
                                    ID_Object = globals.is_GUID(searchItm.searchText) ? searchItm.searchText : null,
                                    Name_Object = globals.is_GUID(searchItm.searchText) ? null : searchItm.searchText,
                                    ID_Parent_Object = cls.GUID_Parent
                                }));
                            }
                            else
                            {
                                objectRelSearch.Add(new clsObjectRel
                                {
                                    ID_Object = globals.is_GUID(searchItm.searchText) ? searchItm.searchText : null,
                                    Name_Object = globals.is_GUID(searchItm.searchText) ? null : searchItm.searchText,
                                });
                            }
                        }
                    }
                });

                if (attributeTypeSearch.Any())
                {
                    List<clsObjectAtt> search = null;

                    if (attributeTypeSearch.Any() && classSearch.Any() && objectSearch.Any())
                    {
                        search = (from attTypeS in attributeTypeSearch
                                  from objectS in objectSearch
                                  from classS in classSearch
                                  select new clsObjectAtt
                                  {
                                      ID_AttributeType = attTypeS?.GUID,
                                      ID_Object = objectS?.GUID,
                                      ID_Class = classS?.GUID,
                                      Val_Named = attTypeS.GUID_Parent == globals.DType_Bool.GUID ? attTypeS.Val_Bool.ToString() : null,
                                      Val_Date = attTypeS.GUID_Parent == globals.DType_DateTime.GUID ? attTypeS.Val_Date : null,
                                      Val_Int = attTypeS.GUID_Parent == globals.DType_Int.GUID ? attTypeS.Val_Long : null,
                                      Val_Real = attTypeS.GUID_Parent == globals.DType_Real.GUID ? attTypeS.Val_Real : null,
                                      Val_String = attTypeS.GUID_Parent == globals.DType_String.GUID ? attTypeS.Val_String : null
                                  }).ToList();
                    }
                    else if (attributeTypeSearch.Any() && classSearch.Any())
                    {
                        search = (from attTypeS in attributeTypeSearch
                                  from classS in classSearch
                                  select new clsObjectAtt
                                  {
                                      ID_AttributeType = attTypeS?.GUID,
                                      ID_Class = classS?.GUID
                                  }).ToList();
                    }
                    else if (attributeTypeSearch.Any() && objectSearch.Any())
                    {
                        search = (from attTypeS in attributeTypeSearch
                                  from objectS in objectSearch
                                  select new clsObjectAtt
                                  {
                                      ID_AttributeType = attTypeS?.GUID,
                                      ID_Object = objectS?.GUID,
                                  }).ToList();
                    }
                    else if (attributeTypeSearch.Any())
                    {
                        search = (from attTypeS in attributeTypeSearch
                                  select new clsObjectAtt
                                  {
                                      ID_AttributeType = attTypeS?.GUID,
                                      Val_Named = attTypeS.GUID_Parent == globals.DType_Bool.GUID ? attTypeS.Val_Bool.ToString() : null,
                                      Val_Date = attTypeS.GUID_Parent == globals.DType_DateTime.GUID ? attTypeS.Val_Date : null,
                                      Val_Int = attTypeS.GUID_Parent == globals.DType_Int.GUID ? attTypeS.Val_Long : null,
                                      Val_Real = attTypeS.GUID_Parent == globals.DType_Real.GUID ? attTypeS.Val_Real : null,
                                      Val_String = attTypeS.GUID_Parent == globals.DType_String.GUID ? attTypeS.Val_String : null
                                  }).ToList();
                    }
                    else if (objectSearch.Any() && classSearch.Any())
                    {
                        search = (from attTypeS in attributeTypeSearch
                                  from classS in classSearch
                                  select new clsObjectAtt
                                  {
                                      ID_AttributeType = attTypeS?.GUID,
                                      ID_Class = classS?.GUID
                                  }).ToList();
                    }
                    else if (objectSearch.Any())
                    {
                        search = (from objectS in objectSearch
                                  select new clsObjectAtt
                                  {
                                      ID_Object = objectS.GUID
                                  }).ToList();
                    }
                    else if (classSearch.Any())
                    {
                        search = (from classS in classSearch
                                  select new clsObjectAtt
                                  {
                                      ID_Class = classS?.GUID
                                  }).ToList();
                    }


                    if (search.Any())
                    {
                        var resultTask = GetAttributeList(search);
                        resultTask.Wait();
                        resultSearch.ItemType = globals.Type_ObjectAtt;
                        resultSearch.AttItems = resultTask.Result.ObjectAttributes;
                    }

                }
                else if (objectRelSearch.Any())
                {
                    if (relationTypeSearch.Any())
                    {
                        objectRelSearch = (from objRel in objectRelSearch
                                           from relType in relationTypeSearch
                                           select new clsObjectRel
                                           {
                                               ID_Object = objRel.ID_Object,
                                               Name_Object = objRel.Name_Object,
                                               ID_Other = objRel.ID_Other,
                                               Name_Other = objRel.Name_Other,
                                               ID_RelationType = relType.GUID
                                           }).ToList();
                    }

                    var resultTaskRelItems = GetObjectRelations(objectRelSearch, direction);
                    resultTaskRelItems.Wait();
                    resultSearch.ItemType = globals.Type_ObjectRel;
                    resultSearch.RelItems = resultTaskRelItems.Result.LookupItems;
                    resultSearch.Direction = resultTaskRelItems.Result.Direction;
                }
                else if (relationTypeSearch.Any())
                {
                    var search = relationTypeSearch.Select(relType => new clsObjectRel
                    {
                        ID_RelationType = relType.GUID
                    }).ToList();

                    var resultTaskRelItems = GetObjectRelations(search, direction);
                    resultTaskRelItems.Wait();
                    resultSearch.ItemType = globals.Type_ObjectRel;
                    resultSearch.RelItems = resultTaskRelItems.Result.LookupItems;
                    resultSearch.Direction = resultTaskRelItems.Result.Direction;
                }
                else if (objectSearch.Any())
                {
                    classSearch.Clear();
                    var classesWithName = objectSearch.Where(obj => string.IsNullOrEmpty(obj.GUID_Parent) && !string.IsNullOrEmpty(obj.Name_Parent)).Select(obj => new clsOntologyItem
                    {
                        Name = obj.Name_Parent
                    }).ToList();

                    if (classesWithName.Any())
                    {

                        var resultTaskClasses = GetClassList(classesWithName);
                        resultTaskClasses.Wait();
                        resultSearch.ItemType = globals.Type_Class;
                        resultSearch.SimpleItems = resultTaskClasses.Result.Classes;
                    }
                    else
                    {

                        var resultTaskObjects = GetObjectList(objectSearch);
                        resultTaskObjects.Wait();
                        resultSearch.ItemType = globals.Type_Object;
                        resultSearch.SimpleItems = resultTaskObjects.Result.Objects;
                    }


                }
                else if (classSearch.Any())
                {
                    var resultTaskObjects = GetObjectList(classSearch);
                    resultTaskObjects.Wait();
                    resultSearch.ItemType = globals.Type_Object;
                    resultSearch.SimpleItems = resultTaskObjects.Result.Objects;
                }


                return resultSearch;
            });

            return taskResult;
        }


        public ServiceAgentElastic(Globals globals)
        {
            this.globals = globals;
        }
    }

    public class LookupSimpleItem
    {
        public List<clsOntologyItem> LookupItems { get; set; }
        public SearchItem SearchItem { get; set; }
    }

    public class LookupRelItem
    {
        public clsOntologyItem Result { get; set; }
        public List<clsObjectRel> LookupItems { get; set; }
        public clsOntologyItem Direction { get; set; }
    }

    public class ResultSimpleItem
    {
        public clsOntologyItem Result { get; set; }
        public LookupSimpleItem SimpleItem { get; set; }
    }

    public class ResultAttributeList
    {
        public clsOntologyItem Result { get; set; }
        public List<clsObjectAtt> ObjectAttributes { get; set; }
    }

    public class ResultClassList
    {
        public clsOntologyItem Result { get; set; }
        public List<clsOntologyItem> Classes { get; set; }
    }

    public class ResultObjectList
    {
        public clsOntologyItem Result { get; set; }
        public List<clsOntologyItem> Objects { get; set; }
    }

   public class ResultSearchItem
    {
        public clsOntologyItem Result { get; set; }
        public string ItemType { get; set; }
        public clsOntologyItem Direction { get; set; }
        public List<clsOntologyItem> SimpleItems { get; set; }
        public List<clsOntologyItem> Classes { get; set; }
        public List<clsObjectAtt> AttItems { get; set; }
        public List<clsObjectRel> RelItems { get; set; }
    }
}
